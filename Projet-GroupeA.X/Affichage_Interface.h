/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */


#ifndef _AFFICHAGE_INTERFACE_H
#define	_AFFICHAGE_INTERFACE_H

#include "utils.h"

/**
 * Genere le repere.
 */
void repereTemp(void);

/**
 * Affiche la temperature dans le cradre.
 * @param Temperature la temperature au format flottant.
 * @param mode l'unite de temperature.
 */
void afficheTempHaut(REAL Temperature, DisplayMode Mode);

/**
 * Vide la colonne d'indice x et y place un pixel a la hauteur y.
 * @param y coordonne y au format flottant.
 * @param x coordonnee x au format entier non signee.
 */
void traceValeur(REAL y, INT8U x);

#endif
