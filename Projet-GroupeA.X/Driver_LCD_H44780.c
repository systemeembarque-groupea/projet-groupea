/****************************************************************************/
/*  GROUPE A Syst�mes embarqu�s                                   ISIMA     */
/*  Novembre 2022                                                           */
/*                                                                          */
/*       Driver de l'afficheur LCD 2 lignes x 16 caracteres                 */
/*       avec controleur graphique Hitachi 44780, interface 4 bits          */
/*                                                                          */
/* Driver_LCD_H44780.c              MPLAB X                    PIC 18F542   */
/****************************************************************************/
#define _Driver_LCD_H44780_C
#include <xc.h>
#include "TypesMacros.h"
#include "Driver_LCD_H44780.h"

// Format de la connectique :  6 bits sur port PORTB
//       Data(7:4) = PORTB(3:0)
//       RS = PORTB(4)
//       E = PORTB(5)

//================================================================   
//      Fonctions d'envoi de commandes ou de donnees
//================================================================

// Les commandes doivent toutes etre separees d'un delai de 5ms (au moins 4ms)
// Les quartets ou octets doivent etre separes par un "pulse" sur la 
// broche LCD_E, d'une duree d'1 micro seconde (au moins 450 ns)

// Envoi d'une commande en mode 8 bits
void LCD_SendCmd8(INT8U Instruction) {
    INT8U Commande_Emise = 0;
/*
Recuperation des bits de poids fort pour ne pas les ecraser :
Cf. remarque sur la l'optimisation concernant la gestion des broches
dans le fichier "Driver_LCD_H44780.h"
*/
    LCD_RS = 0;
    Commande_Emise |= (LCD_PORT_IN & 0xc0); // LCD_PORT_IN = PortB
/* 
Puis ajout des 6 bits forts de
l'instruction (sur les poids faibles)
*/
    Commande_Emise |= (Instruction>>4);
// Emission effective de cette commande, par ecriture sur l'integralite du port
    LCD_PORT_OUT = Commande_Emise;
// Impulsion sur "Enable" pour valider cette commande
    LCD_Pulse_E();
// Attente obligatoire avant re-envoi d'une autre commande
    Delai_ms(5);
}
                                
// Envoi d'une commande en mode 4 bits : RS = 0 
// Transfert Big Endian : D(7:4)= poids fort puis D(7:4)= poids faible
void LCD_SendCmd(INT8U Instruction) {
    INT8U Commande_Emise = 0;
    LCD_RS = 0;
    Commande_Emise = (LCD_PORT_IN & 0xF0);
    Commande_Emise |= (Instruction>>4);
    LCD_PORT_OUT = Commande_Emise;
    LCD_Pulse_E();
    Delai_ms(5);
    
    Commande_Emise = (LCD_PORT_IN & 0xF0);
    Commande_Emise |= (Instruction & 0x0F);
    LCD_PORT_OUT = Commande_Emise;
    LCD_Pulse_E();
    Delai_ms(5);
}

// Envoi d'une donnee en mode 4 bits : RS = 1 
// Transfert Big Endian : D(7:4)= poids fort puis D(7:4)= poids faible
void LCD_SendData(INT8U Donnee) {
    INT8U Donnee_emise = 0;
    LCD_RS = 1;
    Donnee_emise = (LCD_PORT_IN & 0xF0);
    Donnee_emise |= (Donnee>>4);
    LCD_PORT_OUT = Donnee_emise;
    LCD_Pulse_E();
    Delai_ms(5);
    
    Donnee_emise = (LCD_PORT_IN & 0xF0);
    Donnee_emise |= (Donnee & 0x0F);
    LCD_PORT_OUT = Donnee_emise;
    LCD_Pulse_E();
    Delai_ms(5);
}

// Generation d'un front sur la broche LCD_E (PORTB(5)) servant a la validation
void LCD_Pulse_E(void) { 
  LCD_E = 1;   // PORTB|=0b00100000;
  Delai_us(1); // attente d'au moins 450 ns (1 micro seconde)
  LCD_E = 0;   //PORTB&=0b11011111; // front descendant effectif
}


//================================================================   
//  Initialisation du LCD externe sur plateforme MikroElectronika
//================================================================

void LCD_Init(void) {
/*
Initialisation generale :
a) orientation des signaux de controle et des signaux de donnee 
   SANS changer les orientations des autres broches
b) mise a 0 par defaut sur les broches (RS, E, et les donnees)
*/
    TRISB &= 0b11000000;
    LATB = PORTB & 0b11000000;
  // Attente d'au moins 15ms pour permettre le demarrage du LCD : "Power On"
  Delai_ms(16);
  
  // 0x30 : Forcage du LCD en mode 8 bits, 3 fois de suite (avec RS=0)
  LCD_SendCmd8(LCD_EIGHT_BIT);
  LCD_SendCmd8(LCD_EIGHT_BIT);
  LCD_SendCmd8(LCD_EIGHT_BIT);
  
  // 0x20 : Passage en mode 4 bits
  LCD_SendCmd8(LCD_FOUR_BIT);
  
  // A partir de maintenant, les commandes doivent etre passees en mode 4 bits Big Endian
  // ------------------------------------------------------------------------------------
  
  // 0x28 : Taille interface, Nombre de lignes et Taille fonte
  //        4 bits (DL=0), 2 lignes (N=1), caracteres 5x7 (N=0)
  LCD_SendCmd(LCD_TWO_LINES_5x7);

  // 0x0C : Mise en marche de l'affichage (sans curseur ni clignotement)
  LCD_SendCmd(LCD_DISP_ON);

  // 0x06 : Le curseur laisse le texte a gauche, l'affichage n'accompagne pas le deplacement
  LCD_SendCmd(LCD_SHIFT_DISP_LEFT);
  
  // 0x80: Positionne la memoire d'affichage en adresse A(6:0)=000 0000
  LCD_SendCmd(LCD_SET_DDRAM_ADDRESS|0x00);
  
  // 0x01 : "Clear Display"
  LCD_SendCmd(LCD_CLEAR);
}

//================================================================   
//               Fonctions d'ecriture sur LCD
//================================================================

// Positionne l'afficheur a l'adresse DD RAM indiquee en ligne/colonne
void LCD_SetAddress(INT8U row, INT8U column) {
  // Securite sur les donnees fournies : 
  row &= 0x01;    // ne conserve que le bit de poids faible
  column &= 0x0f; // ne conserve que la partie basse

  INT8U address = 0;
  /*
Calcul l'adresse d'affichage : A noter : row introduit un 0x40
  row ...
*/
  if( row != 0)
      address = 0x40; //Si on demande la 2nd ligne, on ecrit � partir de 0x40;
  address |= column;

    /*Generation de la commande pour se positionner
    a la bonne adresse dans la memoire d'affichage*/
LCD_SendCmd(LCD_SET_DDRAM_ADDRESS|address);	

}

// Ecriture d'un texte a la position indiquee
void LCD_WriteAt(INT8U row, INT8U column, CHAR *text) {  
  LCD_SetAddress(row,column); // Positionne effectivement
  while(*text) {
    LCD_Chr(*text); // Ecriture effective sur LCD
    text++;         // Increment
  }
}

// Ecriture d'un texte a la position courante
void LCD_Write(CHAR *text) { 
  while(*text) {
    LCD_Chr(*text); // Ecriture effective sur LCD
    text++;         // Increment
  }
}

// Affichage d'un caractere a la position indiquee
void LCD_ChrAt(INT8U row, INT8U column, CHAR out_char) { // Affichage d'un caractere en ligne et colonne
  LCD_SetAddress(row,column); // Positionne effectivement
  LCD_SendData(out_char);     // Ecriture effective du caractere a cet endroit
}

// Affichage d'un caractere a la position courante
// defini par #define LCD_Chr(Caractere) LCD_SendData(Caractere)

