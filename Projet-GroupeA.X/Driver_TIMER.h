/* 
 * File:   Driver_TIMER.h
 * Author: lorusak
 *
 * Created on 19 septembre 2022, 17:13
 */


#ifndef _DRIVER_TIMER_H
#define	_DRIVER_TIMER_H

#include "ConfigBits_P18F.h"
#include "TypesMacros.h"

#define STACK_SIZE 90

void Timer_Init(void);
void sample(void);
void __interrupt() InterruptManager(void);

#endif
