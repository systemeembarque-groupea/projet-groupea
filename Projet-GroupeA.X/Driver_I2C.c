/****************************************************************************/
/*  GROUPE A Syst�mes embarqu�s                                   ISIMA     */
/*  Novembre 2022                                                           */
/*                                                                          */
/*                      Driver de la liaison I2C                            */
/*                                                                          */
/* Driver_I2C.c                     MPLAB X                    PIC 18F542   */
/****************************************************************************/

#include <xc.h>

#include "TypesMacros.h" 

void I2C_Init(void) {
    // Cablage liaison I2C est : SCL = PORTC(3)
    //                           SDA = PORTC(4)
    TRISCbits.RC3 = 0;
    TRISCbits.RC4 = 0;
    LATCbits.LATC3 = 1;
    LATCbits.LATC4 = 1;
    
    // Configuration du Registre de controle 1 : SSPCON1
    //     0 : WCOL sans collision
    //     0 : SSPOV sans overflow
    //     1 : SSPEN pour mise en marche du MSSP en autorisant SDA et SCL
    //     0 : CKP pas utilise en mode maitre
    //     1000 : SSPM en mode maitre  (I2C Master mode)
    SSPCON1 = 0b00101000;

    // Configuration du Registre de controle 2 : SSPCON2
    SSPCON2 = 0x00; // Initialisation de tous les conditions

    // Configuration du registre d'etat : SSPSTAT
    SSPSTAT = 0b10000000; // pas de Slew rate (vitesse lente 100kHz) 

    // Configuration du registre d'adresse / baud rate generator : SSPADD
    // Calcul du nombre permettant d'obtenir la bonne frequence sur SCL
	
    /*Configuration du baud rate generator : SSPADD
    (voir section 15.4.7)
    sachant que FOSC = 8Mhz, et la frequence clock 
    que l'on cherche a obtenir est = 100kHz*/
    SSPADD = 0x9F ;
}

void I2C_AckSlave(void) {
    // Attente de la fin du transfert precedent: SSPSTAT.R_W
    // Attente de la reception de l'ack par l'esclave
    while ((SSPSTATbits.R_W) || (SSPCON2bits.ACKSTAT));
}

void I2C_Ack(void) {
    SSPCON2bits.ACKDT = 0; // 0 = ACK
    SSPCON2bits.ACKEN = 1; // envoi de la valeur
    // Attente de la reception de l'ack par le maitre 
    while (SSPCON2bits.ACKEN);
}

void I2C_NAck(void) {
    SSPCON2bits.ACKDT = 1; // 1 = NACK
    SSPCON2bits.ACKEN = 1; // envoi de la valeur
    // Attente de la reception de l'ack par le maitre
    while (SSPCON2bits.ACKEN);
}

void I2C_Start(void) {
    SSPCON2bits.SEN = 1;
    while(SSPCON2bits.SEN);
}

void I2C_Stop(void) {
    SSPCON2bits.PEN = 1;
    while(SSPCON2bits.PEN);
}

void I2C_ReStart(void) {
    SSPCON2bits.RSEN = 1;
    while(SSPCON2bits.RSEN);
}

void I2C_Write(INT8U Data) {
    SSPBUF = Data; // Ecriture des donnees dans le buffer
    while (SSPSTATbits.BF); // Attendre tant que le buffer est plein
}

INT8U I2C_Read(void) {
    SSPCON2bits.RCEN = 1; // Autorise la reception sur bus I2C
    while (!SSPSTATbits.BF) ; // Attendre que le buffer se remplisse
    return (SSPBUF); // Lecture des donnees du buffer
}
