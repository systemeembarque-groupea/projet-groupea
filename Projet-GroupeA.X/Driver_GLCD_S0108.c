/****************************************************************************/
/*  MESNARD Emmanuel                                               ISIMA    */
/*  Octobre 2020                                                            */
/*                                                                          */
/*                                                                          */
/*        Driver pour afficheur Graphique LCD (GLCD)                        */
/*        compatible controleur Samsung S0108, SB0108, KS0108               */
/*                                                                          */
/* Driver_GLCD_S0108.c              MPLAB X                    PIC 18F542   */
/****************************************************************************/

// Compatible avec le Graphical LCD Xiamen Ocular GDM12864B de la carte
// EasyPIC 6.0


#include "Driver_GLCD_S0108.h"
#include "Polices.h"

// Variables globales
extern ROM_INT8U *Font;    // Police en cours d'utilisation
extern INT8U FontWidth, FontHeight, FontSpace, FontOffset; 
extern INT8U CP_Line, CP_Row; // CP : current position

//================================================================   
//      Fonctions d'envoi de commandes ou de donnees
//================================================================

void GLCD_SendCmd(INT8U Instruction) {
    GLCD_RS = 0;  GLCD_RW = 0; //Command mode
    GLCD_Data_OUT = Instruction;
    TRIS_GLCD_Data = 0;
    GLCD_Pulse_E();
    Delai_us(5);
}

void GLCD_SendData(INT8U Donnee) {
    GLCD_RS = 1;  GLCD_RW = 0; // Data mode
    GLCD_Data_OUT = Donnee;
    TRIS_GLCD_Data = 0;
    GLCD_Pulse_E();
    Delai_us(5);
}

// Generation d'un front sur la broche GLCD_E (PORTB(4)) servant a la validation
void GLCD_Pulse_E(void) { 
    GLCD_E = 1; 
    Delai_us(1); // Attente duree d'1 micro seconde (au moins 450 ns)
    GLCD_E = 0;  // front descendant effectif
}

// Initialisation du GLCD
void GLCD_Init(void) {
    //orientation des broches,
    //attente power on,
    //gestion du RST
    Delay_ms(15);
    
    TRISB = 0;
    TRISD = 0;
    
    PORTBbits.RB0 = 1;
    PORTBbits.RB1 = 1;
    Delay_ms(2);
    PORTBbits.RB5 = 1;
    Delay_ms(2);
    
    while ((GLCD_ReadStatus()&0x10)==0x10);
    // Pour terminer, envoi de quelques commandes
    GLCD_SendCmd(GLCD_DISP_OFF);            // Normalement, deja effectue
    GLCD_SendCmd(GLCD_SET_START_LINE|0x00); // Idem
    GLCD_SendCmd(GLCD_SET_PAGE|0x00);       // Positionne les adresses a 0
    GLCD_SendCmd(GLCD_SET_COLUMN|0x00);
    GLCD_SendCmd(GLCD_DISP_ON);             // Allume et efface l'ecran
    GLCD_ClrScreen();

    GLCD_SetFont(Police3x6,3,6,1,32);  
}

//================================================================   
//      Fonctions de lecture de l'etat ou de donnees
//================================================================   

// Lecture de l'etat : 0b Busy 0 Disp Rst 0 0 0 0
//         si Busy = 0 : "Ready", sinon "In operation"
//         si Disp = 0 : "Disp On", sinon "Disp Off"
//         si Rst = 0 : "Normal", sinon "In Reset"
INT8U GLCD_ReadStatus(void) {
    INT8U CurrentStatus;

    GLCD_RS = 0;  GLCD_RW = 1; // Status Read
    GLCD_Data_OUT = 0xFF;      // Efface le bus
    TRIS_GLCD_Data = 0xFF;
    GLCD_E = 1;
    Delai_us(5);
    CurrentStatus = GLCD_Data_IN; // Lecture du bus
    GLCD_E = 0;
    Delai_ms(15);
    
    return CurrentStatus;
} 
 
// Lecture d'une donnee
INT8U GLCD_ReadData(void) {
    INT8U CurrentData;

    GLCD_RS = 1;  GLCD_RW = 1;   // Data Read
    GLCD_Data_OUT = 0xFF;        // Efface le bus
    TRIS_GLCD_Data = 0xFF;
    GLCD_E = 1;
    CurrentData = GLCD_Data_IN;  // Lecture du bus
    GLCD_E = 0;
    
    return CurrentData;
} 

//================================================================   
//      Fonctions basiques : position et remplissage   
//================================================================   

// Positionne en X et Y
void GLCD_SetPositionXY(INT8U X_Page, INT8U Y_Column) {   
    GLCD_SendCmd(GLCD_SET_X_ADDRESS|X_Page);
    GLCD_SendCmd(GLCD_SET_Y_ADDRESS|Y_Column);
}   
   
// Remplissage total de l'ecran avec motif
void GLCD_Fill(INT8U Sprite) {
    INT8U x;   // Page x sur 3 bits
    INT8U y;   // Colonne y sur 6 bits   

    GLCD_CS1 = 1;   // Remplissage des deux cotes a la fois
    GLCD_CS2 = 1;   
    for (x=0; x<8; x++) { 
        GLCD_SetPositionXY(x,0x00);
        for(y=0;y<64;y++) GLCD_SendData(Sprite);
    }   
}   

// Effacement ecran
// definit par #define GLCD_ClrScreen() GLCD_Fill(0x00)


//================================================================   
//      Fonctions de gestion d'un pixel : Set et Clr    
//================================================================   

// Dessine un pixel dans l'espace 64 x 128  (XX x YY)
void GLCD_SetPixel(INT8U XX, INT8U YY) {
    INT8U x;      // Page x sur 3 bits
    INT8U y;      // Colonne y sur 6 bits  
    INT8U BitNbr; // Numero de bit sur l'octet considere
    INT8U octet;  // Octet de donnee lu a l'adresse indiquee
    INT8U inter = 1;
    x = XX/8;
    BitNbr = XX%8;

    // Calcul de la position y (Colonne, gauche ou droite)
    if (YY < 64) {
        GLCD_CS1 = 0;  GLCD_CS2 = 1;  // Partie gauche
        y = YY;
    } else {
        GLCD_CS1 = 1;  GLCD_CS2 = 0;  // Partie droite
        y = YY - 64;
    }

    GLCD_SetPositionXY(x,y);
    octet=GLCD_ReadData();
    GLCD_SetPositionXY(x,y); 
    octet=GLCD_ReadData();

    inter <<= BitNbr ;

    octet |= inter;

    GLCD_SetPositionXY(x,y);
    GLCD_SendData(octet); // Mise a jour de la valeur
}

void GLCD_ClrPixel(INT8U XX, INT8U YY) {
    INT8U x;      // Page x sur 3 bits
    INT8U y;      // Colonne y sur 6 bits  
    INT8U BitNbr; // Numero de bit sur l'octet considere
    INT8U octet;  // Octet de donnee lu a l'adresse indiquee
    INT8U inter = 1;
    x = XX/8;
    BitNbr = XX%8;

    // Calcul de la position y (Colonne, gauche ou droite)
    if (YY < 64) {
        GLCD_CS1 = 0;  GLCD_CS2 = 1;  // Partie gauche
        y = YY;
    } else {
        GLCD_CS1 = 1;  GLCD_CS2 = 0;  // Partie droite
        y = YY - 64; 
    }

    GLCD_SetPositionXY(x,y);
    octet=GLCD_ReadData();
    GLCD_SetPositionXY(x,y); 
    octet=GLCD_ReadData();

    inter <<= BitNbr ;

    octet &= ~inter;

    GLCD_SetPositionXY(x,y);
    GLCD_SendData(octet); // Mise a jour de la valeur
}

//================================================================   
//      Fonctions de gestion des lignes droites   
//================================================================  

void GLCD_VerticalLine(INT8U XX1, INT8U YY, INT8U XX2, INT8U Color) {
    INT8U XXdeb, XXfin; // Remise en ordre des XX1 et XX2
    INT8U x;      // Page x sur 3 bits
    INT8U y;      // Colonne y sur 6 bits  
    INT8U BitNbr; // Numero de bit sur l'octet considere
    INT8U octet;  // Octet de donnee lu a l'adresse indique
    INT8U iXX; 

    if(XX1<=XX2){
        XXfin = XX2;
        XXdeb = XX1;
    } else{
        XXfin = XX1;
        XXdeb = XX2;
    }
    x = XXdeb/8;

    //while()
    for (iXX = XXdeb; iXX < XXfin; iXX++) {
        if(Color) {
            GLCD_SetPixel(iXX,YY);
        } else {
            GLCD_ClrPixel(iXX,YY);
        }
    }
}

void GLCD_HorizontalLine(INT8U XX, INT8U YY1, INT8U YY2, INT8U Color) {
    INT8U YYdeb, YYfin; // Remise en ordre des YY1 et YY2
    INT8U iYY; 

    if(YY1 <= YY2) {
        YYfin = YY2;
        YYdeb = YY1;
    } else {
        YYfin = YY1;
        YYdeb = YY2;
    }

    for(iYY = YYdeb; iYY < YYfin+1; iYY++) {
        if (Color) {
            GLCD_SetPixel(XX,iYY);

        } else {
            GLCD_ClrPixel(XX,iYY);
        }
    }
}


void GLCD_Rectangle(INT8U XX1, INT8U YY1, INT8U XX2, INT8U YY2, INT8U Color) {
    // Rectangle vide
    GLCD_VerticalLine(XX1,YY1,XX2,Color);
    GLCD_VerticalLine(XX1,YY2,XX2,Color);
    GLCD_HorizontalLine(XX1,YY1,YY2,Color);
    GLCD_HorizontalLine(XX2,YY1,YY2,Color);
}

//================================================================   
//      Fonction de gestion des caracteres et des textes
//================================================================   

// Choix de la police
// Choix de la police
void GLCD_SetFont(ROM_INT8U *Police, INT8U Largeur, INT8U Hauteur, INT8U Espace, INT8U Offset) {
    Font=Police;            // Police en cours d'utilisation
    FontHeight = Hauteur; 
    FontWidth = Largeur;
    FontSpace = Espace;
    FontOffset = Offset;
    CP_Line = 0;            // Position courante
    CP_Row = 0;
}

// Ecriture d'un texte
void GLCD_PrintAt(INT8U Ligne, INT8U Y, CHAR *Texte, INT8U Color) {
    CP_Line = Ligne;
    CP_Row = Y;
    while (*Texte) {
        GLCD_SetPositionXY(CP_Line,CP_Row);
        GLCD_Chr(*Texte,Color); // Ecriture effective sur LCD
        Texte++;                // Caractere suivant
        CP_Row += FontSpace;
    }
}
// Ecriture d'un caractere en position courante
void GLCD_Chr(CHAR Caractere, INT8U Color) {
    INT8U tmpReadData;    //Contiendra la valeur de l'octet avant ecriture
    INT8U tmpFinalData;   //Contiendra la valeur final a� donner a� l'octet
    INT8U x;
    INT8U y;
    int i;
    
    for (i=0; i<FontWidth; i++){ //Parcours des colonnes du caractere a� ecrire
    
        CP_Row += 1;
        
        if (CP_Row < 64) {
            GLCD_CS1 = 0;  GLCD_CS2 = 1;  // Partie gauche
            y = CP_Row;
        } else {
            GLCD_CS1 = 1;  GLCD_CS2 = 0;  // Partie droite
            y = CP_Row - 64; 
        }
        
        x = CP_Line;

        GLCD_SetPositionXY(x,y);      // Forcage ecriture precedent octet
        tmpReadData=GLCD_ReadData();
        GLCD_SetPositionXY(x,y);      // Lecture effective
        tmpReadData=GLCD_ReadData();
        
        tmpFinalData = Font[(Caractere-FontOffset)*FontWidth+i]; //Octet du caractere a� ecrire
        
        if (Color == 0) {  //Si on ecrit en pixels foncés
            tmpFinalData = ~tmpFinalData & tmpReadData;
        } else {             //Si on ecrit en pixels clairs
            tmpFinalData |= tmpReadData;
        }
        
        GLCD_SetPositionXY(x,y);
        GLCD_SendData(tmpFinalData); // Mise a jour de la valeur
    }
}
