#include <stdio.h>
#include <string.h>

#include "ConfigBits_P18F.h"
#include "TypesMacros.h"
#include "Affichage_Interface.h"
#include "Driver_GLCD_S0108.h"
#include "Polices.h"

#define axeX1 10
#define axeX2 60
#define axeY1 10
#define axeY2 100
#define coinX1 0
#define coinY1 100
#define coinX2 coinX1+8
#define coinY2 coinY1+26

void repereTemp(void){
    INT8U j;
    
    // Generation axe ordonnee de 50 pixels de haut, afin de couvrir des temperatures de -10 � 40�C.
    GLCD_VerticalLine(axeX1, axeY1, axeX2, 1);
    // Axe des abscisses de 100 pixels de long afin d'afficher une grosse quantite de valeurs.
    GLCD_HorizontalLine(axeX2-10, axeY1, axeY2, 1);
    
    for (j=axeX1; j<axeX2+1; j+=5){
        // Generation de graduations tous les 5�C.
        GLCD_HorizontalLine(j, axeY1-4, axeY1, 1);
    }
}

void afficheTempHaut(REAL Temperature, DisplayMode Mode){
    GLCD_PrintAt(0, coinY1+1, "######", 0); // on vide le cadre
    CHAR Temp[8];
    
    sprintf(Temp, "%.1f", Temperature); // Conversion temperature en string
    switch(Mode){
        case CELSIUS:
            strcat(Temp, "~C");
            break;
        case FARENHEIT:
            strcat(Temp, "~F");
            break;
        case KELVIN:
            strcat(Temp, "K");
            break;
    }
    GLCD_Rectangle(coinX1, coinY1, coinX2, coinY2, 1);
    GLCD_PrintAt(0, coinY1+1, Temp, 1);
}

void traceValeur(REAL y, INT8U x){
    INT16 val = (INT16) y;
    
    // On va vider la colonne d'indice x et la colonne d'indice x+1.
    GLCD_VerticalLine(axeX1, axeY1+x+1, axeX2-11 ,0);
    GLCD_VerticalLine(axeX2-9, axeY1+x+1, axeX2, 0);
    GLCD_VerticalLine(axeX1 ,axeY1+x+2, axeX2-11, 0);
    GLCD_VerticalLine(axeX2-9, axeY1+x+2, axeX2, 0);
    GLCD_VerticalLine(axeX1, axeY1+x+3, axeX2-11, 0);
    GLCD_VerticalLine(axeX2-9, axeY1+x+3, axeX2, 0);
    
    if (y > 40)
        val = 40;
    else if (y < -10)
        val = -10;
    
    GLCD_SetPixel(axeX2-10-((INT8U)val), axeY1+x+1);
}
