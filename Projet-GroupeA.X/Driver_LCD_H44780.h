/****************************************************************************/
/*  GROUPE A Syst�mes embarqu�s                                   ISIMA     */
/*  Novembre 2020                                                           */
/*                                                                          */
/*       Driver de l'afficheur LCD 2 lignes x 16 caracteres                 */
/*       avec controleur graphique Hitachi 44780, interface 4 bits          */
/*                                                                          */
/* Driver_LCD_H44780.h              MPLAB X                    PIC 18F542   */
/****************************************************************************/

#ifndef	_Driver_LCD_H44780_H
#define _Driver_LCD_H44780_H
#include <xc.h>
#include "TypesMacros.h"

//================================================================   
//      Connection des broches du LCD sur carte EasyPIC 6.0
//================================================================

// Format de l'afficheur :
//    Memoire d'affichage DD RAM
//
//                        |     V i s i b l e     | I n v i s i b l e  |   
//              Ligne 0 : | 00 01 02 03 04 ... 0F | 10 11 12   ...  27 |  
//              Ligne 1 : | 40 41 42 ...       4F | 50 51 52   ...  67 |
//
//    Memoire du generateur de caractere CG RAM
//        Contient au maximum 8 caracteres ou symboles 5x7.
//        La CG RAM utilise des mots de 8 bits de large, mais seul les 5 bits
//        de poids faible apparaissent sur le LCD.
//        Exemple : 0x1F allume tous les points de cette rangee
//                  0x00 eteint tous ces points.
//
// Format de la connectique :  6 bits sur port PORTB
//       Data(7:4) = PORTB(3:0)
//       RS = PORTB(4)
//       E = PORTB(5)

#define LCD_D4      LATBbits.LATB0   // Data port : donnee ou instruction
#define TRIS_LCD_D4 TRISBbits.TRISB0
#define LCD_D5      LATBbits.LATB1
#define TRIS_LCD_D5 TRISBbits.TRISB1
#define LCD_D6      LATBbits.LATB2
#define TRIS_LCD_D6 TRISBbits.TRISB2
#define LCD_D7      LATBbits.LATB3
#define TRIS_LCD_D7 TRISBbits.TRISB3

#define LCD_RS      LATBbits.LATB4   // Register Select : 0 pour une instruction, 
#define TRIS_LCD_RS TRISBbits.TRISB4 //                   1 pour une donnee

#define LCD_E       LATBbits.LATB5   // Enable : pulse pour valider
#define TRIS_LCD_E  TRISBbits.TRISB5
/*
Plutot que de gerer les 6 bits un par un sur ce port B,
l'ensemble du port B (les 8 bits) sera parfois traite simultanement...
Il faudra alors prendre soin de sauvegarder les 2 bits de poids
fort (non utilises par le LCD), avant de reecrire sur le port
les 6 nouveaux bits et les 2 bits sauvegardes*/

#define LCD_PORT_IN   PORTB
#define LCD_PORT_OUT  LATB
  
//================================================================   
//      Jeu d'instructions standard
//================================================================

// "Clear" et "Return home"
#define LCD_CLEAR       0b00000001  // Efface la DD RAM
#define LCD_RETURN_HOME 0b00000010  // Retourne en position origine

// "Entry Mode Set" [0 0 0 0 0 1 l/D S ] :
//    Mode de fonctionnement de l'affichage et du curseur :
//    Sens de deplacement du curseur  = vers la droite apres caractere (l/D=1)
//    L'affichage n'accompagne pas le deplacement (S=0)
#define LCD_SHIFT_CUR_LEFT    0b00000100  // Deplacement du curseur a gauche
#define LCD_SHIFT_CUR_RIGHT   0b00000101  // Deplacement du curseur a droite
#define LCD_SHIFT_DISP_LEFT   0b00000110  // Decalage de l'affichage a gauche
#define LCD_SHIFT_DISP_RIGHT  0b00000111  // Decalage de l'affichage a droite

// "Display Control" [0 0 0 0 1 D C B] : 
//    Affichage en fonction (Display D=1),
//    sans curseur (Cursor C=0),
//    ni clignotement (Blink B=0)
#define LCD_DISP_ON       0b00001100  // Affichage en marche, sans curseur
#define LCD_DISP_OFF      0b00001000  // Desactivation de l'affichage
#define LCD_CUR_ON        0b00001110  // Affichage en marche, avec curseur
#define LCD_CUR_OFF       0b00001100  // Affichage, sans curseur
#define LCD_BLINK_ON      0b00001101  // Clignotement
#define LCD_BLINK_OFF     0b00001100  // Desactivation du Clignotement
#define LCD_CUR_BLINK_ON  0b00001111  // Affichage avec curseur clignotant
#define LCD_CUR_BLINK_OFF 0b00001110  // Affichage avec curseur sans clignotant
#define LCD_NO_CURSOR     0b00001100  // Affichage en marche, sans curseur

// "Cursor and display shift" [0 0 0 1 S/C R/L x x] :
//    Decalage du curseur et de l'affichage
//    Deplace le curseur (S/C=1) ou l'affichage (S/C=0) d'une position
//    vers la gauche (R/L=1) ou la droite (R/L=0) sans changer la DD RAM
#define LCD_MOVE_CUR_LEFT   0b00011100  // Decalage curseur a gauche
#define LCD_MOVE_CUR_RIGHT  0b00011000  // Decalage curseur a droite
#define LCD_MOVE_DISP_LEFT  0b00010100  // Decalage affichage a gauche
#define LCD_MOVE_DISP_RIGHT 0b00010000  // Decalage affichage a droite

// "Function Set" [0 0 1 DL N F X X] :
// Format de l'interface : Mode de transfert, nombre de lignes et taille fonte
//    Mode de l'interface (DL=0 pour mode 4 bits, DL=1 pour mode 8 bits),
//    Nombre de lignes (N=0 pour 1 ligne, N=1 pour 2 ou 4 lignes),
//    Taille des fontes(F=0 pour des caracteres 5x7, F=1 pour des caracteres 5x10)
#define LCD_FOUR_BIT       0b00100000  // Interface 4-bit
#define LCD_EIGHT_BIT      0b00110000  // Interface 8-bit
#define LCD_ONE_LINE_5x7   0b00100000  // Une ligne en police 5x7
#define LCD_TWO_LINES_5x7  0b00101000  // Plusieurs lignes en police 5x7
#define LCD_ONE_LINE_5X10  0b00100100  // Une ligne en police 5x10
#define LCD_TWO_LINES_5x10 0b00101100  // Plusieurs lignes en police 5x10

// "Set CG RAM address" [0 1 A5 A4 A3 A2 A1 A0] :
// Fixe l'adresse A(5:0) d'acces a la CG RAM 
#define	LCD_SET_CGRAM_ADDRESS 0b01000000

// "Set DD RAM address" [1 A6 A5 A4 A3 A2 A1 A0] :
// Fixe l'adresse A(6:0) d'acces a la DD RAM 
#define	LCD_SET_DDRAM_ADDRESS 0b10000000

//================================================================   
//      Fonctions de gestion du LCD
//================================================================

// Envoi d'une commande ou d'une donnee, en mode 4 ou 8 bits 
void LCD_SendCmd8(INT8U Instruction);
void LCD_SendCmd(INT8U Instruction);
void LCD_SendData(INT8U Donnee);

void LCD_Pulse_E(void); // Front montant puis descendant sur E
void LCD_Init(void);    // Initialisation du LCD

//================================================================   
//               Fonctions d'ecriture sur LCD
//================================================================

// Positionne l'afficheur a l'adresse DD RAM indiquee en ligne/colonne
void LCD_SetAddress(INT8U row, INT8U column);

// Ecriture d'un texte
void LCD_WriteAt(INT8U row, INT8U column, CHAR *text);
void LCD_Write(CHAR *text);
# define LCD_PrintAt LCD_WriteAt
# define LCD_Print LCD_Write

// Affichage d'un caractere
void LCD_ChrAt(INT8U row, INT8U column, CHAR out_char);
#define LCD_Chr(Caractere) LCD_SendData(Caractere)

#endif