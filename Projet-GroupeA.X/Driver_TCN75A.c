/****************************************************************************/
/*  GROUPE A Systemes embarques                                   ISIMA     */
/*  Novembre 2022                                                           */
/*                                                                          */
/*                                                                          */
/*            Driver pour le capteur MicroChip TCN75A en mode I2C           */
/*            installe sur la carte Microbot MR003-001.2 (MR312)            */
/*                                                                          */
/* Driver_TCN75A.c                  MPLAB X                    PIC 18F542   */
/****************************************************************************/

#define _Driver_TCN75A_C

#include <xc.h>

#include "Driver_I2C.h"
#include "Driver_TCN75A.h"
#include "TypesMacros.h"

void TCN75A_Init(void) {
    I2C_Init();

    Delai_ms(5); // Delai de prise en compte, optionnel

    TCN75A_Write8(TEMP_CONFIG, 0b11100000);

    Delai_ms(2); // Delai pour demarrage : voir Power-Up
}

void TCN75A_Write8(INT8U Registre, INT8U Donnee) {
    I2C_Start();
    I2C_Write(CHIP_Write);
    I2C_AckSlave();
    I2C_Write(Registre);
    I2C_AckSlave();
    I2C_Write(Donnee);
    I2C_AckSlave();
    I2C_Stop();
}

void TCN75A_Write(INT8U Registre, INT16 Donnee) {
    I2C_Start();
    I2C_Write(CHIP_Write);
    I2C_AckSlave();
    I2C_Write(Registre);
    I2C_AckSlave();
    I2C_Write((INT8U) Donnee >> 8);
    I2C_AckSlave();
    I2C_Write((INT8U) Donnee >> 0);
    I2C_AckSlave();
    I2C_Stop();
}

INT8U TCN75A_Read8(INT8U Registre) {
    INT8U ValeurLue;
    
    I2C_Start();
    I2C_Write(CHIP_Write);
    I2C_AckSlave();
    I2C_Write(Registre);
    I2C_AckSlave();
    I2C_ReStart();
    I2C_Write(CHIP_Read);
    I2C_AckSlave();
    ValeurLue = I2C_Read();
    I2C_NAck();
    I2C_Stop();
    
    return ValeurLue;
}

INT16 TCN75A_Read(INT8U Registre) {
    INT16U ValeurLue;
    
    I2C_Start();
    I2C_Write(CHIP_Write);
    I2C_AckSlave();
    I2C_Write(Registre);
    I2C_AckSlave();
    I2C_ReStart();
    I2C_Write(CHIP_Read);
    I2C_AckSlave();
    ValeurLue = ((INT16U)I2C_Read()) << 8;
    I2C_Ack();
    ValeurLue |= (INT16U)I2C_Read();
    I2C_NAck();
    I2C_Stop();
    
    return (INT16) ValeurLue;
}

void TCN75A_GetTemperature8(INT8U *Tmp_MSB, INT8U *Tmp_LSB) {
    INT16U ValeurLue;
    
    ValeurLue = (INT16U) TCN75A_Read((INT8U) TEMP_AMBIENT);
    
    *Tmp_MSB = ValeurLue >> 8;
    *Tmp_LSB = ValeurLue & 0x00FF;
}

INT16 TCN75A_GetTemperature_Fixed(void) {
    return TCN75A_Read(TEMP_AMBIENT) / 0b1000;
}

INT8 TCN75A_GetTemperature_Integer(void) {
    return (INT8) (TCN75A_Read(TEMP_AMBIENT) >> 8);
}

REAL TCN75A_GetTemperature_Float(void) {
    return ((float) TCN75A_GetTemperature_Fixed()) /  32.0f;
}

void TCN75A_Sleep(void) {
    INT8U config = TCN75A_Read8(TEMP_CONFIG);
    TCN75A_Write8(TEMP_CONFIG, config | 0b00000001);
}

void TCN75A_WakeUp(void) {
    INT8U config = TCN75A_Read8(TEMP_CONFIG);
    TCN75A_Write8(TEMP_CONFIG, config & 0b11111110);
}

int TCN75A_IsSleeping(void) {
    INT8U config = TCN75A_Read8(TEMP_CONFIG);
    int isSleeping = (int) (config | 0x01);
    return isSleeping;
}

void TCN75A_ComparativeMode(int polarity) {
    INT8U config = TCN75A_Read8(TEMP_CONFIG);
    if (polarity)
        TCN75A_Write8(TEMP_CONFIG, config | 0b00000110);
    else
        TCN75A_Write8(TEMP_CONFIG, config | 0b00000010);
}

void TCN75A_OneShot(void) {
    INT8U config = TCN75A_Read8(TEMP_CONFIG);
    TCN75A_Write8(TEMP_CONFIG, config | 0b10000001);
    Delay_ms(30);
}

void TCN75A_SetTempLimit(INT16 limit) {
    TCN75A_Write(TEMP_SET_LIMIT,limit);
}

void TCN75A_SetHisteresis(INT16 limit) {
    TCN75A_Write(TEMP_HYST,limit);
}

void TCN75A_InterruptMode(void) {
    INT8U config = TCN75A_Read8(TEMP_CONFIG);
    TCN75A_Write8(TEMP_CONFIG, config & 0b11111101);
}
