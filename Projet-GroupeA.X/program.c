/* 
 * File:   program.c
 * Author: tolhopital
 *
 * Created on 14 novembre 2022, 17:40
 */


#include <stdio.h>
#include <xc.h>

#include "TypesMacros.h"
#include "ConfigBits_P18F.h"
#include "Driver_TCN75A.h"
#include "Driver_GLCD_S0108.h"
#include "Driver_TIMER.h"
#include "Affichage_Interface.h"
#include "utils.h"

INT8U stack_head = 0;
float temp_stack[STACK_SIZE];
DisplayMode mode = CELSIUS;
INT8U veille = 0;
INT8U interrupted;

void main() {
    // --- Init --- //
    // Capteur
    TCN75A_Init();
    Delay_ms(200);
    // GLCD
    GLCD_Init();
    Delay_ms(200);
    repereTemp();
    // Interruption manager
    Timer_Init();
    // Configuration boutons
    TRISBbits.TRISB6 = 1;
    TRISBbits.TRISB7 = 1;
    PORTBbits.RB6 = 0;
    PORTBbits.RB7 = 0;
    LATBbits.LATB6 = 0;
    LATBbits.LATB7 = 0;
    
    /*
    while (TRUE) {
        // Récupération température dans pile, mise à jour tête de pile
        temp_stack[stack_head] = TCN75A_GetTemperature_Float();
        // Affiche sur le GLCD texte pur, précision max
        afficheTempHaut(temp_stack[stack_head], mode);
        // Mise à jour graphe GLCD
        traceValeur(temp_stack[stack_head], stack_head);
        stack_head = (stack_head + 1)%STACK_SIZE;
        Delay_ms(250);
    }
    */
    
    while (TRUE) {
        // Changer unité
        if (PORTBbits.RB6 == 1) {
            mode = (mode + 1) % 3;
        }
        
        if (interrupted) {
            sample();
            interrupted = 0;
        }
        
        /*
        if (PORTBbits.RB7 == 1) { // Mode veille
            if (TCN75A_IsSleeping()) {
                TCN75A_WakeUp();
            } else {
                TCN75A_Sleep();
            }
        }
        */

        Delay_ms(250);
    }
}
