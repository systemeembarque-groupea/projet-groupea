/****************************************************************************/
/*  GROUPE A Syst�mes embarqu�s                                   ISIMA     */
/*  Novembre 2022                                                           */
/*                                                                          */
/*                                                                          */
/*            Driver pour le capteur MicroChip TCN75A en mode I2C           */
/*            installe sur la carte Microbot MR003-001.2 (MR312)            */
/*                                                                          */
/* Driver_TCN75A.h                  MPLAB X                    PIC 18F542   */
/****************************************************************************/


#ifndef	_DRIVER_TCN75A_H
#define _DRIVER_TCN75A_H

#include "Driver_I2C.h"
#include "TypesMacros.h"

//============================================
// Definition des Registres du capteur TCN75A
//============================================
#define	TEMP_AMBIENT 0b00
#define	TEMP_CONFIG 0b01
#define TEMP_HYST 0b10
#define	TEMP_SET_LIMIT 0b11

//============================================ 
//        Adresses I2C de cette puce
//============================================ 
#define CHIP_Write 0b10010000
#define CHIP_Read 0b10010001
							 							 
//================================================================   
//  Prototypes des fonctions du driver du capteur TCN75A
//================================================================

void TCN75A_Init(void);
void TCN75A_Write8(INT8U Registre, INT8U Donnee);
void TCN75A_Write(INT8U Registre, INT16 Donnee);
INT8U TCN75A_Read8(INT8U Registre);
INT16 TCN75A_Read(INT8U Registre);
void TCN75A_GetTemperature8(INT8U *T_MSB, INT8U *T_LSB);


/**
 * Recupere aupres du thermometre la temperature au format de virgule fixe signee.
 * @return la valeur sur 16 bits avec les 4 derniers bits �tant apres la virgule (valeur multipliee par 32).
 */
INT16 TCN75A_GetTemperature_Fixed(void);

/**
 * Recupere aupr�s du thermometre la temperature au format entier signee.
 * @return la valeur sur 8 bits.
 */
INT8 TCN75A_GetTemperature_Integer(void);

/**
 * Recupere aupres du thermometre la temperature au format flottant.
 * @return la valeur.
 */
REAL TCN75A_GetTemperature_Float(void);

/**
 * Permet de mettre en veille le capteur de temperature.
 * Consommation d'energie reduite.
 */
void TCN75A_Sleep(void);

/**
 * Permet de sortir le capteur de temperature de sa mise en veille.
 */
void TCN75A_WakeUp(void);

/**
 * Permet de connaitre l'etat du capteur de temperature, en veille ou actif.
 * @return un entier :
 *      1 si il est en veille
 *      0 sinon
 */
int TCN75A_IsSleeping(void);

/**
 * Active le mode comparaison du capteur de temperature.
 * @param polarity On choisit la polarit� du retour qui sera donne par le capteur en cas de depassement. 
 */
void TCN75A_ComparativeMode(int polarity);

/**
 * Utilise le mode One Shot du capteur de temperature.
 * Effectue une seule mesure pendant la veille du capteur.
 * Necessite de lire la temperature par la suite.
 */
void TCN75A_OneShot(void);

/**
 * Donne au capteur la valeur limite pour comparaison et interruption.
 * @param limit valeur temperature limite.
 */
void TCN75A_SetTempLimit(INT16 limit);

/**
 * Donne au capteur la valeur sous laquelle il peut desactiver la comparaison/interruption lors de la redescente en temperature.
 * @param limit
 */
void TCN75A_SetHisteresis(INT16 limit);

/**
 * Active le mode interruption du capteur de temperature.
 * Ce mode permet de generer une interruption si la temperature depasse une valeur donnee.
 */
void TCN75A_InterruptMode(void);


#endif
