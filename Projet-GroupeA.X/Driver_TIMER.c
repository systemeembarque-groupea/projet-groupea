#include <xc.h>
#include "Driver_TIMER.h"
#include "TypesMacros.h"
#include "ConfigBits_P18F.h"
#include "Driver_TCN75A.h"
#include "Affichage_Interface.h"
#include "utils.h"

#define Convert_CtoF(C) ((C) * 9.0f/5.0f + 32.0f)
#define Convert_CtoK(C) ((C) + 273.15f)

extern INT8U stack_head;
extern float temp_stack[STACK_SIZE];
extern DisplayMode mode;
extern INT8U interrupted;

void Timer_Init(void) {
    // Timer 0 Pre-scale 1:8
    INTCONbits.GIE = 1;
    TMR0IE = 1;
    
    TMR0ON = 1;
    T08BIT = 0;
    T0CS = 0;
    T0SE = 0;    
    PSA = 0;
    T0PS2 = 0;
    T0PS1 = 1;
    T0PS0 = 0;
    
    interrupted = 0;
}

void sample(void) {
    // Recuperation temperature dans pile, mise a jour tete de pile
    temp_stack[stack_head] = TCN75A_GetTemperature_Float();
    // Affiche sur le GLCD texte pur, precision max
    switch(mode) {
        case CELSIUS:
            afficheTempHaut(temp_stack[stack_head], mode);
            break;
        case FARENHEIT:
            afficheTempHaut(Convert_CtoF(temp_stack[stack_head]), mode);
            break;
        case KELVIN:
            afficheTempHaut(Convert_CtoK(temp_stack[stack_head]), mode);
            break;
        default:
            break;
    }
    // Mise a jour graphe GLCD
    traceValeur(temp_stack[stack_head], stack_head);
    stack_head = (stack_head + 1) % STACK_SIZE;
}

void __interrupt() InterruptManager(void){
    if (TMR0IE && TMR0IF) {
        TMR0IF = 0;
        interrupted = 1;
    }
}